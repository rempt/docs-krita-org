# Translation of docs_krita_org_reference_manual___dockers___advanced_color_selector.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___advanced_color_selector\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:30+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/dockers/advanced_color_selector.rst:1
msgid "Overview of the advanced color selector docker."
msgstr "Огляд бічної панелі розширеного вибору кольорів."

#: ../../reference_manual/dockers/advanced_color_selector.rst:11
#: ../../reference_manual/dockers/advanced_color_selector.rst:16
msgid "Advanced Color Selector"
msgstr "Розширений вибір кольору"

#: ../../reference_manual/dockers/advanced_color_selector.rst:11
#: ../../reference_manual/dockers/advanced_color_selector.rst:24
msgid "Color Selector"
msgstr "Вибір кольору"

#: ../../reference_manual/dockers/advanced_color_selector.rst:11
msgid "Color"
msgstr "Колір"

#: ../../reference_manual/dockers/advanced_color_selector.rst:20
msgid ".. image:: images/dockers/Advancecolorselector.jpg"
msgstr ".. image:: images/dockers/Advancecolorselector.jpg"

#: ../../reference_manual/dockers/advanced_color_selector.rst:21
msgid ""
"As compared to other color selectors in Krita, Advanced color selector "
"provides more control and options to the user. To open Advanced color "
"selector choose :menuselection:`Settings --> Dockers --> Advanced Color "
"Selector`. You can configure this docker by clicking on the little wrench "
"icon on the top left corner. Clicking on the wrench will open a popup window "
"with following tabs and options:"
msgstr ""
"Порівняно із іншими засобами вибору кольорів у Krita, розширений засіб "
"вибору кольору надає у розпорядження користувача більше засобів керування та "
"параметрів. Щоб відкрити вікно розширеного засобу вибору кольору, "
"скористайтеся пунктом меню :menuselection:`Параметри --> Бічні панелі --> "
"Розширений вибір кольору`. Налаштувати цю бічну панель можна після "
"натискання маленької піктограми із гайковим ключем у верхньому лівому куті "
"панелі. Після натискання піктограми із гайковим ключем програма відкриє "
"контекстне вікно із такими вкладками та пунктами:"

#: ../../reference_manual/dockers/advanced_color_selector.rst:26
msgid "Here you configure the main selector."
msgstr "Тут ви можете налаштувати основний засіб вибору."

#: ../../reference_manual/dockers/advanced_color_selector.rst:28
msgid "Show Color Selector"
msgstr "Показати панель вибору кольору"

#: ../../reference_manual/dockers/advanced_color_selector.rst:32
msgid ""
"This allows you to configure whether to show or hide the main color selector."
msgstr ""
"Надає вам змогу визначити, чи буде показано головний засіб вибору кольору."

#: ../../reference_manual/dockers/advanced_color_selector.rst:35
msgid "Type and Shape"
msgstr "Тип і форма"

#: ../../reference_manual/dockers/advanced_color_selector.rst:38
msgid ".. image:: images/dockers/Krita_Color_Selector_Types.png"
msgstr ".. image:: images/dockers/Krita_Color_Selector_Types.png"

#: ../../reference_manual/dockers/advanced_color_selector.rst:39
msgid ""
"Here you can pick the hsx model you'll be using. There's a small blurb "
"explaining the characteristic of each model, but let's go into detail:"
msgstr ""
"Тут можна вибрати модель HSX, якою ви користуватиметеся. Передбачено "
"невеличкі підказки, які пояснюють основні характеристики кожної з моделей, "
"але розберемо усе докладніше:"

#: ../../reference_manual/dockers/advanced_color_selector.rst:42
msgid "HSV"
msgstr "HSV"

#: ../../reference_manual/dockers/advanced_color_selector.rst:43
msgid ""
"Stands for Hue, Saturation, Value. Saturation determines the difference "
"between white, gray, black and the most colorful color. Value in turn "
"measures either the difference between black and white, or the difference "
"between black and the most colorful color."
msgstr ""
"Абревіатура від Hue (відтінок), Saturation (насиченість), Value (значення). "
"Насиченість визначає відмінність між білим, сірим, чорним та більш яскравими "
"кольорами. Значення є виміром або відмінності між чорним і білим, або "
"відмінності між чорним і яскравішим кольором."

#: ../../reference_manual/dockers/advanced_color_selector.rst:44
msgid "HSL"
msgstr "HSL"

#: ../../reference_manual/dockers/advanced_color_selector.rst:45
msgid ""
"Stands for Hue, Saturation, Lightness. All saturated colors are equal to 50% "
"lightness. Saturation allows for shifting between gray and color."
msgstr ""
"Абревіатура від Hue (відтінок), Saturation (насиченість), Lightness "
"(освітленість). Усі насичені кольори мають значення освітленості 50%. "
"Насиченість надає змогу визначити місце кольору між сірим і насиченим."

#: ../../reference_manual/dockers/advanced_color_selector.rst:46
msgid "HSI"
msgstr "HSI"

#: ../../reference_manual/dockers/advanced_color_selector.rst:47
msgid ""
"This stands for Hue, Saturation and Intensity. Unlike HSL, this one "
"determine the intensity as the sum of total rgb components. Yellow (1,1,0) "
"has higher intensity than blue (0,0,1) but is the same intensity as cyan "
"(0,1,1)."
msgstr ""
"Абревіатура від Hue (відтінок), Saturation (насиченість) та Intensity "
"(інтенсивність). На відміну від HSL, тут інтенсивність визначається як сума "
"значень компонентів RGB. Жовтий колір (1,1,0) має вищу інтенсивність за "
"синій (0,0,1), але таку саму інтенсивність як блакитний (0,1,1)."

#: ../../reference_manual/dockers/advanced_color_selector.rst:49
msgid "HSY'"
msgstr "HSY'"

#: ../../reference_manual/dockers/advanced_color_selector.rst:49
msgid ""
"Stands for Hue, Saturation, Luma, with Luma being an RGB approximation of "
"true luminosity. (Luminosity being the measurement of relative lightness). "
"HSY' uses the Luma Coefficients, like `Rec. 709 <https://en.wikipedia.org/"
"wiki/Rec._709>`_, to calculate the Luma. Due to this, HSY' can be the most "
"intuitive selector to work with, or the most confusing."
msgstr ""
"Абревіатура від Hue (відтінок), Saturation (насиченість), Luma (яскравість). "
"Тут яскравість є наближенням у RGB справжньої світності. (Світність є "
"виміром відносної освітленості). У HSY' використовують коефіцієнти "
"яскравості, зокрема `Rec 709 <https://en.wikipedia.org/wiki/Rec._709>`_, для "
"обчислення яскравості. Через це, HSY' може бути найкращим з точки зору "
"інтуїтивності засобом вибору кольору або найгіршим."

#: ../../reference_manual/dockers/advanced_color_selector.rst:51
msgid ""
"Then, under shape, you can select one of the shapes available within that "
"color model."
msgstr ""
"Далі, під формою ви можете вибрати одну з форм, які є доступними для моделі "
"кольорів."

#: ../../reference_manual/dockers/advanced_color_selector.rst:55
msgid ""
"Triangle is in all color models because to a certain extent, it is a "
"wildcard shape: All color models look the same in an equilateral triangle "
"selector."
msgstr ""
"Трикутник використовують для усіх моделей кольорів, оскільки, до певної "
"міри, це універсальна форма: усі моделі кольорів виглядають однаково на "
"засобі вибору кольору за допомогою рівнобічного трикутника."

#: ../../reference_manual/dockers/advanced_color_selector.rst:58
msgid "Luma Coefficients"
msgstr "Коефіцієнти яскравості"

#: ../../reference_manual/dockers/advanced_color_selector.rst:60
msgid ""
"This allows you to edit the Luma coefficients for the HSY model selectors to "
"your leisure. Want to use `Rec. 601 <https://en.wikipedia.org/wiki/Rec."
"_601>`_ instead of Rec. 709? These boxes allow you to do that!"
msgstr ""
"За допомогою цих пунктів ви можете редагувати коефіцієнти яскравості для "
"засобів вибору для моделі HSY так, як вам завгодно. Хочете скористатися "
"`Rec. 601 <https://en.wikipedia.org/wiki/Rec._601>`_ замість Rec 709? За "
"допомогою цих пунктів ви можете це зробити!"

#: ../../reference_manual/dockers/advanced_color_selector.rst:62
msgid "By default, the Luma coefficients should add up to 1 at maximum."
msgstr ""
"Типово, коефіцієнти яскравості у сумі мають давати число, яке не перевищує 1."

#: ../../reference_manual/dockers/advanced_color_selector.rst:65
msgid "Gamma"
msgstr "Гама"

#: ../../reference_manual/dockers/advanced_color_selector.rst:65
msgid ""
"The HSY selector is linearised, this setting allows you to choose how much "
"gamma is applied to the Luminosity for the gui element. 1.0 is fully linear, "
"2.2 is the default."
msgstr ""
"Засіб вибору HSY лінеаризовано. За допомогою цього пункту ви можете вибрати, "
"яку частину гами буде застосовано до світності для елемента графічного "
"інтерфейсу. 1.0 — повністю лінійний, 2.2 — типовий."

#: ../../reference_manual/dockers/advanced_color_selector.rst:68
msgid "Color Space"
msgstr "Простір кольорів"

#: ../../reference_manual/dockers/advanced_color_selector.rst:70
msgid ""
"This allows you to set the overall color space for the Advanced Color "
"Selector."
msgstr ""
"Цей інструмент надає вам змогу встановити загальний простір кольорів для "
"засобу розширеного вибору кольору."

#: ../../reference_manual/dockers/advanced_color_selector.rst:73
msgid ""
"You can pick only sRGB colors in advanced color selector regardless of the "
"color space of advanced color selector. This is a bug."
msgstr ""
"У розширеному засобі вибору кольору ви можете вибрати лише кольори sRGB, "
"незалежно від простору кольорів розширеного засобу вибору кольору. Це вада "
"програми."

#: ../../reference_manual/dockers/advanced_color_selector.rst:76
msgid "Behavior"
msgstr "Поведінка"

#: ../../reference_manual/dockers/advanced_color_selector.rst:79
msgid "When docker resizes"
msgstr "При зміні розмірів панелі"

#: ../../reference_manual/dockers/advanced_color_selector.rst:81
msgid "This determines the behavior of the widget as it becomes smaller."
msgstr "Визначає поведінку віджета при зменшенні."

#: ../../reference_manual/dockers/advanced_color_selector.rst:83
msgid "Change to Horizontal"
msgstr "Змінити на горизонтальне"

#: ../../reference_manual/dockers/advanced_color_selector.rst:84
msgid ""
"This'll arrange the shade selector horizontal to the main selector. Only "
"works with the MyPaint shade selector."
msgstr ""
"За допомогою цього пункту можна розташувати засіб вибору відтінків "
"горизонтально на основній панелі засобу вибору. Працює лише із засобом "
"вибору відтінків MyPaint."

#: ../../reference_manual/dockers/advanced_color_selector.rst:85
msgid "Hide Shade Selector."
msgstr "Приховати вибір відтінку."

#: ../../reference_manual/dockers/advanced_color_selector.rst:86
msgid "This hides the shade selector."
msgstr "Приховує засіб вибору відтінків."

#: ../../reference_manual/dockers/advanced_color_selector.rst:88
msgid "Do nothing"
msgstr "Нічого не робити"

#: ../../reference_manual/dockers/advanced_color_selector.rst:88
msgid "Does nothing, just resizes."
msgstr "Не робити нічого, просто змінити розмір."

#: ../../reference_manual/dockers/advanced_color_selector.rst:91
msgid "Zoom selector UI"
msgstr "Панель вибору масштабу"

#: ../../reference_manual/dockers/advanced_color_selector.rst:93
msgid ""
"If your have set the docker size considerably smaller to save space, this "
"option might be helpful to you. This allows you to set whether or not the "
"selector will give a zoomed view of the selector in a size specified by you, "
"you have these options for the zoom selector:"
msgstr ""
"Якщо ви встановили значно менший розмір бічної панелі для заощадження місця, "
"цей пункт може бути корисним. За його допомогою ви можете визначити, чи "
"надаватиме засіб вибору вам масштабований перегляд із визначеним вами "
"масштабом. Ви можете вибрати такі варіанти для засобу масштабування:"

#: ../../reference_manual/dockers/advanced_color_selector.rst:95
msgid "when pressing middle mouse button"
msgstr "при натисканні середньої кнопки миші"

#: ../../reference_manual/dockers/advanced_color_selector.rst:96
msgid "on mouse over"
msgstr "при наведенні вказівника"

#: ../../reference_manual/dockers/advanced_color_selector.rst:97
msgid "never"
msgstr "ніколи"

#: ../../reference_manual/dockers/advanced_color_selector.rst:99
msgid ""
"The size given here, is also the size of the Main Color Selector and the "
"MyPaint Shade Selector when they are called with the :kbd:`Shift + I` and :"
"kbd:`Shift + M` shortcuts, respectively."
msgstr ""
"Визначений тут розмір є також розміром основної панелі вибору кольору та "
"панелі вибору відтінків MyPaint, коли ви їх викликаєте за допомогою "
"комбінацій клавіш :kbd:`Shift + I` і :kbd:`Shift + M`, відповідно."

#: ../../reference_manual/dockers/advanced_color_selector.rst:102
msgid "Hide Pop-up on click"
msgstr "Ховати контекстне вікно після клацання"

#: ../../reference_manual/dockers/advanced_color_selector.rst:102
msgid ""
"This allows you to let the pop-up selectors called with the above hotkeys to "
"disappear upon clicking them instead of having to leave the pop-up boundary. "
"This is useful for faster working."
msgstr ""
"За допомогою цього пункту ви можете дозволити програмі приховувати після "
"клацання контекстні панелі вибору, які викликають за допомогою описаних вище "
"комбінацій клавіш, замість залишення їх збоку від панелі вибору кольору. "
"Корисно для пришвидшення роботи."

#: ../../reference_manual/dockers/advanced_color_selector.rst:105
msgid "Shade selector"
msgstr "Вибір відтінку"

#: ../../reference_manual/dockers/advanced_color_selector.rst:107
msgid ""
"Shade selector options. The shade selectors are useful to decide upon new "
"shades of color."
msgstr ""
"Параметри засобу вибору відтінку. Засоби вибору відтінку корисні для вибору "
"серед нових відтінків кольору."

#: ../../reference_manual/dockers/advanced_color_selector.rst:111
msgid "Update Selector"
msgstr "Вибір варіанта оновлення"

#: ../../reference_manual/dockers/advanced_color_selector.rst:113
msgid "This allows you to determine when the shade selector updates."
msgstr ""
"За допомогою цього пункту ви можете визначити, коли слід оновлювати дані у "
"засобі вибору відтінку."

#: ../../reference_manual/dockers/advanced_color_selector.rst:116
msgid "MyPaint Shade Selector"
msgstr "Інструмент вибору відтінків MyPaint"

#: ../../reference_manual/dockers/advanced_color_selector.rst:118
msgid ""
"Ported from MyPaint, and extended with all color models. Default hotkey is :"
"kbd:`Shift + M`."
msgstr ""
"Портовано з MyPaint і розширено на усі моделі кольорів. Типовим клавіатурним "
"скороченням для виклику є :kbd:`Shift + M`."

#: ../../reference_manual/dockers/advanced_color_selector.rst:122
msgid "Simple Shade Selector"
msgstr "Простий засіб вибору відтінків"

#: ../../reference_manual/dockers/advanced_color_selector.rst:124
msgid "This allows you to configure the simple shade selector in detail."
msgstr "Надає вам змогу точно налаштувати простий засіб вибору відтінків."

#: ../../reference_manual/dockers/advanced_color_selector.rst:127
msgid "Color Patches"
msgstr "Набори кольорів"

#: ../../reference_manual/dockers/advanced_color_selector.rst:129
msgid "This sets the options of the color patches."
msgstr "Встановлює параметри наборів кольорів."

#: ../../reference_manual/dockers/advanced_color_selector.rst:131
msgid ""
"Both Color History and Colors From the Image have similar options which will "
"be explained below."
msgstr ""
"Обидва засоби, журнал кольорів і кольори з зображення, мають однакові "
"параметри, пояснення щодо яких наведено нижче."

#: ../../reference_manual/dockers/advanced_color_selector.rst:133
msgid "Show"
msgstr "Показати"

#: ../../reference_manual/dockers/advanced_color_selector.rst:134
msgid ""
"This is a radio button to show or hide the section. It also determines "
"whether or not the colors are visible with the advanced color selector "
"docker."
msgstr ""
"Це перемикач показу або приховування розділу. Він також визначає те, чи "
"будуть кольори видимими на бічній панелі розширеного засобу вибору кольору."

#: ../../reference_manual/dockers/advanced_color_selector.rst:135
msgid "Size"
msgstr "Розмір"

#: ../../reference_manual/dockers/advanced_color_selector.rst:136
msgid "The size of the color boxes can be set here."
msgstr "За допомогою цього пункту можна визначити розмір квадратиків кольорів."

#: ../../reference_manual/dockers/advanced_color_selector.rst:137
msgid "Patch Count"
msgstr "Кількість наборів"

#: ../../reference_manual/dockers/advanced_color_selector.rst:138
msgid "The number of patches to display."
msgstr "Кількість пунктів кольорів, які слід показувати."

#: ../../reference_manual/dockers/advanced_color_selector.rst:139
msgid "Direction"
msgstr "Напрямок"

#: ../../reference_manual/dockers/advanced_color_selector.rst:140
msgid "The direction of the patches, Horizontal or Vertical."
msgstr "Напрямок наборів, горизонтальний або вертикальний."

#: ../../reference_manual/dockers/advanced_color_selector.rst:141
msgid "Allow Scrolling"
msgstr "Дозволити гортання"

#: ../../reference_manual/dockers/advanced_color_selector.rst:142
msgid ""
"Whether to allow scrolling in the section or not when there are too many "
"patches."
msgstr ""
"Визначає, чи буде уможливлено гортання розділу, якщо у ньому надто багато "
"пунктів кольорів."

#: ../../reference_manual/dockers/advanced_color_selector.rst:143
msgid "Number of Columns/Rows"
msgstr "Кількість стовпчиків/рядків"

#: ../../reference_manual/dockers/advanced_color_selector.rst:144
msgid "The number of Columns or Rows to show in the section."
msgstr "Кількість стовпчиків або рядків, які слід показувати у цьому розділі."

#: ../../reference_manual/dockers/advanced_color_selector.rst:146
msgid "Update After Every Stroke"
msgstr "Оновлювати після кожного мазка"

#: ../../reference_manual/dockers/advanced_color_selector.rst:146
msgid ""
"This is only available for Colors From the Image and tells the docker "
"whether to update the section after every stroke or not, as after each "
"stroke the colors will change in the image."
msgstr ""
"Цей пункт доступний лише для кольорів з екрана. За його допомогою можна "
"визначити, чи оновлюватиме програма розділ після кожного мазка, як після "
"кожного мазка змінюється колір на зображенні."

#: ../../reference_manual/dockers/advanced_color_selector.rst:149
msgid "History patches"
msgstr "Журнал кольорів"

#: ../../reference_manual/dockers/advanced_color_selector.rst:151
msgid ""
"The history patches remember which colors you've drawn on canvas with. They "
"can be quickly called with the :kbd:`H` key."
msgstr ""
"У журналі кольорів зберігаються кольори, якими ви малювали на полотні. "
"Швидко викликати панель журналу можна за допомогою натискання клавіші :kbd:"
"`H`."

#: ../../reference_manual/dockers/advanced_color_selector.rst:154
msgid "Common Patches"
msgstr "Типовий набір"

#: ../../reference_manual/dockers/advanced_color_selector.rst:156
msgid ""
"The common patches are generated from the image, and are the most common "
"color in the image. The hotkey for them on canvas is the :kbd:`U` key."
msgstr ""
"Типовий набір створюється на основі зображення, до нього програма включає "
"найтиповіші кольори зображення. Викликати панель типового набору можна "
"натисканням клавіші :kbd:`U`."

#: ../../reference_manual/dockers/advanced_color_selector.rst:159
msgid "Gamut masking"
msgstr "Маскування діапазону кольорів"

#: ../../reference_manual/dockers/advanced_color_selector.rst:165
msgid ""
"Gamut masking is available only when the selector shape is set to wheel."
msgstr ""
"Маскуванням діапазону кольорів можна скористатися, лише якщо для форми "
"засобу вибору кольорів встановлено значення «коло»."

#: ../../reference_manual/dockers/advanced_color_selector.rst:167
msgid ""
"You can select and manage your gamut masks in the :ref:`gamut_mask_docker`."
msgstr ""
"Вибирати маски діапазону кольорів та керувати такими масками можна за "
"допомогою :ref:`бічної панелі масок діапазону кольорів <gamut_mask_docker>`."

#: ../../reference_manual/dockers/advanced_color_selector.rst:169
msgid ""
"In the gamut masking toolbar at the top of the selector you can toggle the "
"selected mask off and on (left button). You can also rotate the mask with "
"the rotation slider (right)."
msgstr ""
"За допомогою панелі керування маскою діапазону кольорів, розташованої у "
"верхній частині панелі вибору кольору, ви можете увімкнути або вимкнути "
"вибрану маску (ліва кнопка). Ви також можете обертати маску за допомогою "
"повзунка обертання (праворуч)."

#: ../../reference_manual/dockers/advanced_color_selector.rst:174
msgid "External Info"
msgstr "Сторонні відомості"

#: ../../reference_manual/dockers/advanced_color_selector.rst:176
msgid ""
"`HSI and HSY for Krita’s advanced color selector. <https://wolthera.info/?"
"p=726>`_"
msgstr ""
"`HSI і HSY для засобу розширеного вибору кольору Krita. <https://wolthera."
"info/?p=726>`_"
