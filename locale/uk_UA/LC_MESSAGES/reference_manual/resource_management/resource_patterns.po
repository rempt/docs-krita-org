# Translation of docs_krita_org_reference_manual___resource_management___resource_patterns.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___resource_management___resource_patterns\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:47+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/resource_management/resource_patterns.rst:None
msgid ".. image:: images/patterns/Krita_Patterns.png"
msgstr ".. image:: images/patterns/Krita_Patterns.png"

#: ../../reference_manual/resource_management/resource_patterns.rst:None
msgid ".. image:: images/patterns/Generating_custom_patterns1.png"
msgstr ".. image:: images/patterns/Generating_custom_patterns1.png"

#: ../../reference_manual/resource_management/resource_patterns.rst:None
msgid ".. image:: images/patterns/Generating_custom_patterns2.png"
msgstr ".. image:: images/patterns/Generating_custom_patterns2.png"

#: ../../reference_manual/resource_management/resource_patterns.rst:None
msgid ".. image:: images/patterns/Generating_custom_patterns3.png"
msgstr ".. image:: images/patterns/Generating_custom_patterns3.png"

#: ../../reference_manual/resource_management/resource_patterns.rst:1
msgid "Creating and managing patterns in Krita."
msgstr "Створення візерунків і керування візерунками у Krita."

#: ../../reference_manual/resource_management/resource_patterns.rst:11
#: ../../reference_manual/resource_management/resource_patterns.rst:16
msgid "Patterns"
msgstr "Візерунки"

#: ../../reference_manual/resource_management/resource_patterns.rst:11
msgid "Resources"
msgstr "Ресурси"

#: ../../reference_manual/resource_management/resource_patterns.rst:21
msgid ""
"Patterns are small raster files that tile. They can be used as following:"
msgstr ""
"Візерунки — малі растрові файли, які можна використовувати як елементи "
"мозаїки. Їх можна використовувати"

#: ../../reference_manual/resource_management/resource_patterns.rst:23
msgid "As fill for a vector shape."
msgstr "Як заповнення для векторної форми."

#: ../../reference_manual/resource_management/resource_patterns.rst:24
msgid "As fill-tool color."
msgstr "Як колір для інструмента заповнення."

#: ../../reference_manual/resource_management/resource_patterns.rst:25
msgid "As height-map for a brush using the 'texture' functionality."
msgstr ""
"Як карту висот для пензля, у якому використано функціональну можливість "
"«текстури»."

#: ../../reference_manual/resource_management/resource_patterns.rst:26
msgid "As fill for a generated layer."
msgstr "Як заповнення для створеного шару."

#: ../../reference_manual/resource_management/resource_patterns.rst:29
msgid "Adding new patterns"
msgstr "Додавання нових візерунків"

#: ../../reference_manual/resource_management/resource_patterns.rst:31
msgid ""
"You can add new patterns via the pattern docker, or the pattern-quick-access "
"menu in the toolbar. At the bottom of the docker, beneath the resource-"
"filter input field, there are the :guilabel:`Import resource` and :guilabel:"
"`Delete resource` buttons. Select the former to add png or jpg files to the "
"pattern list."
msgstr ""
"Додати нові візерунки можна за допомогою бічної панелі візерунків або меню "
"швидкого доступу до візерунків на панелі інструментів. У нижній частині "
"бічної панелі візерунків під полем для введення тексту для фільтрування "
"ресурсів передбачено кнопки :guilabel:`Імпортувати ресурс` та :guilabel:"
"`Вилучити ресурс`. Натисніть кнопку імпортування, щоб додати до списку "
"візерунків візерунок з файла png або jpg."

#: ../../reference_manual/resource_management/resource_patterns.rst:34
msgid ""
"Similarly, removing patterns can be done by pressing the :guilabel::`Delete "
"resource` button. Krita will not delete the actual file then, but rather "
"black list it, and thus not load it."
msgstr ""
"Так само, вилучити візерунок можна натисканням кнопки :guilabel:`Вилучити "
"ресурс`. Krita не вилучатиме сам файл, а лише додасть відповідний візерунок "
"до «чорного» списку і не завантажуватиме його."

#: ../../reference_manual/resource_management/resource_patterns.rst:37
msgid "Temporary patterns and generating patterns from the canvas"
msgstr ""
"Тимчасові візерунки і створення візерунків на основі зображення на полотні"

#: ../../reference_manual/resource_management/resource_patterns.rst:39
msgid ""
"You can use the pattern drop-down to generate patterns from the canvas but "
"also to make temporary ones."
msgstr ""
"Ви можете скористатися спадним списком візерунків для створення візерунків "
"на основі зображення на полотні, а також для створення тимчасових візерунків."

#: ../../reference_manual/resource_management/resource_patterns.rst:41
msgid "First, draw a pattern and open the pattern drop-down."
msgstr "Спочатку, намалюйте візерунок і відкрийте спадний список візерунків."

#: ../../reference_manual/resource_management/resource_patterns.rst:46
msgid ""
"Then go into :guilabel:`custom` and first press :guilabel:`Update` to show "
"the pattern in the docker. Check if it's right. Here, you can also choose "
"whether you use this layer only, or the whole image. Since 3.0.2, Krita will "
"take into account the active selection as well when getting the information "
"of the two."
msgstr ""
"Далі, перейдіть до пункту :guilabel:`Нетипові` і спочатку натисніть :"
"guilabel:`Оновити`, щоб програма показала пункт візерунка на бічній панелі. "
"Перевірте, чи пункт з'явився. Ви також можете визначити, використовуватимете "
"ви лише поточний шар чи усе зображення. Починаючи з версії 3.0.2, Krita "
"також братиме до уваги поточне позначення при отриманні даних поточного шару "
"або усього зображення."

#: ../../reference_manual/resource_management/resource_patterns.rst:51
msgid ""
"Then, click either :guilabel:`Use as Pattern` to use it as a temporary "
"pattern, or :guilabel:`Add to predefined patterns` to save it into your "
"pattern resources!"
msgstr ""
"Потім натисніть або :guilabel:`Використати як візерунок`, щоб використати "
"ділянку як тимчасовий візерунок, або :guilabel:`Додати до стандартних "
"візерунків`, щоб зберегти візерунок до сховища ресурсів візерунків."

#: ../../reference_manual/resource_management/resource_patterns.rst:53
msgid ""
"You can then start using it in Krita by for example making a canvas and "
"doing :guilabel:`Edit --> Fill with Pattern`."
msgstr ""
"Далі, ви можете використовувати візерунок у Krita, наприклад, створивши "
"полотно і вибравши у меню пункт :guilabel:`Зміни --> Заповнити візерунком`."

#: ../../reference_manual/resource_management/resource_patterns.rst:59
msgid ":ref:`pattern_docker`"
msgstr ":ref:`pattern_docker`"

#: ../../reference_manual/resource_management/resource_patterns.rst:61
msgid "You can tag patterns here, and filter them with the resource filter."
msgstr ""
"Тут ви можете призначити мітку для візерунка і фільтрувати список візерунків "
"за допомогою фільтра ресурсів."
