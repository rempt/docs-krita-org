msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:05\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___colors___profiling_and_callibration.pot\n"

#: ../../<generated>:1
msgid "Soft-proofing"
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:1
msgid "Color Models in Krita"
msgstr "Krita 的色彩模型"

#: ../../general_concepts/colors/profiling_and_callibration.rst:13
msgid "Color"
msgstr "色彩"

#: ../../general_concepts/colors/profiling_and_callibration.rst:13
msgid "Profiling"
msgstr "特性化"

#: ../../general_concepts/colors/profiling_and_callibration.rst:13
msgid "Calibration"
msgstr "校准"

#: ../../general_concepts/colors/profiling_and_callibration.rst:18
msgid "Profiling and Calibration"
msgstr "特性化和校准"

#: ../../general_concepts/colors/profiling_and_callibration.rst:20
msgid ""
"So to make it simple, a color profile is just a file defining a set of "
"colors inside a pure XYZ color cube. This \"color set\" can be used to "
"define different things:"
msgstr ""
"简单来说，色彩特性文件是在 XYZ 参考色彩空间中定义了一组颜色的文件。我们可以使"
"用色彩特性文件来定义："

#: ../../general_concepts/colors/profiling_and_callibration.rst:23
msgid "the colors inside an image"
msgstr "图像中的色彩"

#: ../../general_concepts/colors/profiling_and_callibration.rst:25
msgid "the colors a device can output"
msgstr "设备可以输出的色彩"

#: ../../general_concepts/colors/profiling_and_callibration.rst:27
msgid ""
"Choosing the right workspace profile to use depends on how much colors you "
"need and on the bit depth you plan to use. Imagine a line with the whole "
"color spectrum from pure black (0,0,0) to pure blue (0,0,1) in a pure XYZ "
"color cube. If you divide it choosing steps at a regular interval, you get "
"what is called a linear profile, with a gamma=1 curve represented as a "
"straight line from 0 to 1. With 8bit/channel bit depth, we have only 256 "
"values to store this whole line. If we use a linear profile as described "
"above to define those color values, we will miss some important visible "
"color change steps and have a big number of values looking the same (leading "
"to posterization effect)."
msgstr ""
"要选择一个合适的工作空间特性文件，你首先要确定使用哪些颜色，这些颜色可以被细"
"分成多少种，然后据此决定颜色通道的位深度。现在假设有一条线，它位于 XYZ 参考色"
"彩空间的坐标系上，连接着从纯黑 (0,0,0) 到纯蓝 (0,0,1) 的一系列颜色。我们假设"
"它的 gamma 为 1，意味着它是一条数值从 0 到 1 的直线。我们假设它的颜色通道深度"
"为 8 位，意味着这条直线被等距分成了 255 段，可以容纳 256 个不同的颜色坐标。通"
"过上述的定义，我们便得到了一个线性特性文件。线性空间尽管在数学上准确，却不能"
"很好地反映人眼的感知特性，我们在“Gamma 和线性”一章已经介绍过这个问题。如果在"
"线性空间下面使用每通道 8 位深度，那么在人类最敏感的暗部颜色区域的可用变化级数"
"就会很少，空间大部分的颜色感觉很浅，却在很少几级明度距离中一口气由亮变暗，所"
"以特别容易发生颜色条纹，造成色调分离现象。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:33
msgid ""
"This is why was created the sRGB profile to fit more different colors in "
"this limited amount of values, in a perceptually regular grading, by "
"applying a custom gamma curve (see picture here: https://en.wikipedia.org/"
"wiki/SRGB) to emulate the standard response curve of old CRT screens. So "
"sRGB profile is optimized to fit all colors that most common screen can "
"reproduce in those 256 values per R/G/B channels. Some other profiles like "
"Adobe RGB are optimized to fit more printable colors in this limited range, "
"primarily extending cyan-green hues. Working with such profile can be useful "
"to improve print results, but is dangerous if not used with a properly "
"profiled and/or calibrated good display. Most common CMYK workspace profile "
"can usually fit all their colors within 8bit/channel depth, but they are all "
"so different and specific that it's usually better to work with a regular "
"RGB workspace first and then convert the output to the appropriate CMYK "
"profile."
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:38
msgid ""
"Starting with 16bit/channel, we already have 65536 values instead of 256, so "
"we can use workspace profiles with higher gamut range like Wide-gamut RGB or "
"Pro-photo RGB, or even unlimited gamut like scRGB."
msgstr ""
"我们前面已经提到过 8 位深度的颜色通道只能容纳 256 种颜色，所以 8 位的非 sRGB "
"空间容易在屏幕上产生颜色条纹。而把通道深度提升至 16 位时每条颜色通道可以容纳 "
"65536 种颜色，因此在 16 位或更高位深度下面我们可以使用色域更宽的工作空间特性"
"文件，如 Wide-gamut RGB 或 Pro-photo RGB，甚至无限色域的空间，如 scRGB。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:40
msgid ""
"But sRGB being a generic profile (even more as it comes from old CRT "
"specifications...), there are big chances that your monitor have actually a "
"different color response curve, and so color profile. So when you are using "
"sRGB workspace and have a proper screen profile loaded (see next point), "
"Krita knows that the colors the file contains are within the sRGB color "
"space, and converts those sRGB values to corresponding color values from "
"your monitor profile to display the canvas."
msgstr ""
"然而 sRGB 只是一个通用意义上的特性文件，许多显示器还有它们的特性文件的实际颜"
"色响应曲线并不完全符合 sRGB 规范。但不必担心，在使用 sRGB 工作空间时，只要加"
"载了正确的显示器特性文件，Krita 就会自动在标准 sRGB 特性文件和显示器特性文件"
"之间转换颜色，让显示在画布上的颜色符合 sRGB 规范。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:43
msgid ""
"Note that when you export your file and view it in another software, this "
"software has to do two things:"
msgstr "这也是任何软件打开图像时 **应该** 做的两件事："

#: ../../general_concepts/colors/profiling_and_callibration.rst:45
msgid ""
"read the embed profile to know the \"good\" color values from the file "
"(which most software do nowadays; when they don't they usually default to "
"sRGB, so in the case described here we're safe )"
msgstr ""
"第一步，读取图像嵌入的特性文件，判断哪些颜色可以显示。绝大多数当代软件会做这"
"一步，即使它们不做，也往往会默认文件为 sRGB，这在当代使用情景下面是安全的。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:46
msgid ""
"and then convert it to the profile associated to the monitor (which very few "
"software actually does, and just output to sRGB.. so this can explain some "
"viewing differences most of the time)."
msgstr ""
"第二步，把图像的颜色按照显示器的特性文件进行转换后显示。遗憾的是极少软件会做"
"这一步，大多数软件会按 sRGB 原样输出，因此会造成显示效果的偏差。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:48
msgid "Krita uses profiles extensively, and comes bundled with many."
msgstr "Krita 会尽可能地使用特性文件，它也自带了大量的特性文件。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:50
msgid ""
"The most important one is the one of your own screen. It doesn't come "
"bundled, and you have to make it with a color profiling device. In case you "
"don't have access to such a device, you can't make use of Krita's color "
"management as intended. However, Krita does allow the luxury of picking any "
"of the other bundled profiles as working spaces."
msgstr ""
"然而 Krita 却偏偏没有自带你的显示器的特性文件，因为每台显示器的显示效果都是不"
"同的，你必须使用校色设备自行制作显示器的特性文件，这个过程就叫做显示器的 **特"
"性化** 。显示器特性文件是色彩管理流程中最重要的特性文件，缺少了它，你将无法按"
"照 Krita 的设计意图来发挥它的色彩管理功能。无论如何，这不影响你 Krita 里面任"
"选一种它自带的特性文件作为工作空间。只是为了取得最准确的显示效果，你应该考虑"
"为你的显示器进行校色和特性化。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:54
msgid "Profiling devices"
msgstr "校色设备"

#: ../../general_concepts/colors/profiling_and_callibration.rst:56
msgid ""
"Profiling devices, called Colorimeters, are tiny little cameras of a kind "
"that you connect to your computer via an usb, and then you run a profiling "
"software (often delivered alongside of the device)."
msgstr ""
"校色设备通常是一种叫做 **色差计** 的特殊小型数码相机，另外还有高级一点的 **分"
"光光度计** 。我们以更常见的色差计为例。要进行校色和特性化，先把色差计通过 "
"USB 连接到计算机，然后运行它配套的特性化软件。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:60
msgid ""
"If you don't have software packaged with your colorimeter, or are unhappy "
"with the results, we recommend `ArgyllCMS <https://www.argyllcms.com/>`_."
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:62
msgid ""
"The little camera then measures what the brightest red, green, blue, white "
"and black are like on your screen using a predefined white as base. It also "
"measures how gray the color gray is."
msgstr ""
"色差计的感光元件会在一种预设白的基础上测量最纯的红、绿、蓝、白、黑等颜色在屏"
"幕上的实际显示效果，它也会测量一些中间灰的实际效果。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:64
msgid ""
"It then puts all this information into an ICC profile, which can be used by "
"the computer to correct your colors."
msgstr ""
"测量所得的信息会被存入一个 ICC 特性文件，这便是你的 **显示器特性文件** 了。计"
"算机加载该文件后即可修正显示的颜色。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:66
msgid ""
"It's recommended not to change the \"calibration\" (contrast, brightness, "
"you know the menu) of your screen after profiling. Doing so makes the "
"profile useless, as the qualities of the screen change significantly while "
"calibrating."
msgstr ""
"我们不建议在进行特性化之后更改显示器的显示参数 (对比度、亮度等)。一旦更改，之"
"前制作的特性文件就失去了意义，因为屏幕的显示效果已明显变化。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:68
msgid ""
"To make your screen display more accurate colors, you can do one or two "
"things: profile your screen or calibrate and profile it."
msgstr ""
"要让你的显示器更加精确地显示颜色，你可以有两种校色办法：仅进行特性化或者校准"
"并进行特性化。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:71
msgid ""
"Just profiling your screen means measuring the colors of your monitor with "
"its native settings and put those values in a color profile, which can be "
"used by color-managed application to adapt source colors to the screen for "
"optimal result. Calibrating and profiling means the same except that first "
"you try to calibrate the screen colors to match a certain standard setting "
"like sRGB or other more specific profiles. Calibrating is done first with "
"hardware controls (lightness, contrast, gamma curves), and then with "
"software that creates a vcgt (video card gamma table) to load in the GPU."
msgstr ""
"仅进行特性化：按照显示器的当前设置进行测量，测量得到的数值会被存入设备的色彩"
"特性文件。具有色彩管理能力的应用软件会按照这个特性文件来调整图像源颜色数值在"
"该屏幕上的最佳显示方式。校准并特性化：在进行特性化之前先把显示器调整得尽可能"
"符合某种规范，比如符合 sRGB 规范。校准时首先调整显示器硬件的参数 (亮度、对"
"比、 gamma 曲线等)，然后用软件生成一个 vcgt (显卡 gamma 表，video card gamma "
"table)，加载到 GPU。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:75
msgid "So when or why should you do just one or both?"
msgstr "这两种方法我们应该如何选择呢？"

#: ../../general_concepts/colors/profiling_and_callibration.rst:77
msgid "Profiling only:"
msgstr "仅进行特性化："

#: ../../general_concepts/colors/profiling_and_callibration.rst:79
msgid "With a good monitor"
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:80
msgid ""
"You can get most of the sRGB colors and lot of extra colors not inside sRGB. "
"So this can be good to have more visible colors."
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:82
msgid "With a bad monitor"
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:82
msgid ""
"You will get just a subset of actual sRGB, and miss lot of details, or even "
"have hue shifts. Trying to calibrate it before profiling can help to get "
"closer to full-sRGB colors."
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:84
msgid "Calibration+profiling:"
msgstr "校准并进行特性化："

#: ../../general_concepts/colors/profiling_and_callibration.rst:86
msgid "Bad monitors"
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:87
msgid "As explained just before."
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:88
msgid "Multi-monitor setup"
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:89
msgid ""
"When using several monitors, and specially in mirror mode where both monitor "
"have the same content, you can't have this content color-managed for both "
"screen profiles. In such case, calibrating both screens to match sRGB "
"profile (or another standard for high-end monitors if they both support it) "
"can be a good solution."
msgstr ""

#: ../../general_concepts/colors/profiling_and_callibration.rst:91
msgid ""
"When you need to match an exact rendering context for soft-proofing, "
"calibrating can help getting closer to the expected result. Though switching "
"through several monitor calibration and profiles should be done extremely "
"carefully."
msgstr ""
