# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-28 15:02+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Paint Connection"
msgstr "Verbindingslijn tekenen"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:1
msgid "The Dyna Brush Engine manual page."
msgstr "De handleidingpagina Dyna-penseel-engine."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:15
msgid "Dyna Brush Engine"
msgstr "Dyna-penseel-engine"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:18
msgid ".. image:: images/icons/dynabrush.svg"
msgstr ".. image:: images/icons/dynabrush.svg"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:19
msgid ""
"Dyna brush uses dynamic setting like mass and drag to draw strokes. The "
"results are fun and random spinning strokes. To experiment more with this "
"brush you can play with values in 'dynamic settings' section of the brush "
"editor under Dyna Brush."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:23
msgid ""
"This brush engine has been removed in 4.0. This engine mostly had smoothing "
"results that the dyna brush tool has in the toolbox. The stabilizer settings "
"can also give you further smoothing options from the tool options."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:26
msgid "Options"
msgstr "Opties"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:28
msgid ":ref:`option_size_dyna`"
msgstr ":ref:`option_size_dyna`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:29
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:30
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:31
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:36
msgid "Brush Size (Dyna)"
msgstr "Penseelgrootte (Dyna)"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:39
msgid "Dynamics Settings"
msgstr "Instellingen voor dynamiek"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:41
msgid "Initial Width"
msgstr "Initiële breedte"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:42
msgid "Initial size of the dab."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:43
msgid "Mass"
msgstr "Massa"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:44
msgid "How much energy there is in the satellite like movement."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:45
msgid "Drag"
msgstr "Verslepen"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:46
msgid "How close the dabs follow the position of the brush-cursor."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:48
msgid "Width Range"
msgstr "Breedtebereik"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:48
msgid "How much the dab expands with speed."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:51
msgid "Shape"
msgstr "Vorm"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:53
msgid "Diameter"
msgstr "Diameter"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:54
msgid "Size of the shape."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:55
msgid "Angle"
msgstr "Hoek"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:56
msgid "Angle of the shape. Requires Fixed Angle active to work."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:57
msgid "Circle"
msgstr "Cirkel"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:58
msgid "Make a circular dab appear."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:59
msgid "Two"
msgstr "Twee"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:60
msgid "Draws an extra circle between other circles."
msgstr "Tekent een extra cirkel tussen andere cirkels."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:61
msgid "Line"
msgstr "Lijn"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:62
msgid ""
"Connecting lines are drawn next to each other. The number boxes on the right "
"allows you to set the spacing between the lines and how many are drawn."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:63
msgid "Polygon"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:64
msgid "Draws a black polygon as dab."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:65
msgid "Wire"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:66
msgid "Draws the wireframe of the polygon."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:68
msgid "Draws the connection line."
msgstr ""
