# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-01 17:04+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: layersandmasks ref\n"

#: ../../reference_manual/layers_and_masks.rst:5
msgid "Layers and Masks"
msgstr "Camadas e Máscaras"

#: ../../reference_manual/layers_and_masks.rst:7
msgid "Layers are a central concept in digital painting."
msgstr "As camadas são um conceito central na pintura digital."

#: ../../reference_manual/layers_and_masks.rst:9
msgid ""
"With layers you can get better control over your artwork, for example you "
"can color an entire artwork just by working on the separate color layer and "
"thereby not destroying the line art which will reside above this color layer."
msgstr ""
"Com as camadas, poderá obter um melhor controlo sobre as suas obras; por "
"exemplo, poderá colorir uma obra inteira, bastando para tal trabalhar na "
"camada de cor separada e assim não destruir a imagem que irá residir por "
"cima desta camada de cor."

#: ../../reference_manual/layers_and_masks.rst:11
msgid ""
"Furthermore, layers allow you to change the composition easier, and mass "
"transform certain elements at once."
msgstr ""
"Para além disso, as camadas permitem-lhe mudar mais facilmente a composição "
"e transformar em lote certos elementos de uma vez."

#: ../../reference_manual/layers_and_masks.rst:13
msgid ""
"Masks on the other hand allow you to selectively apply certain effects on a "
"layer, like transparency, transformation and filters."
msgstr ""
"As máscaras, por outro lado, permitem-lhe aplicar de forma selectiva sobre "
"uma camada, como a transparência, alguma transformação ou filtros."

#: ../../reference_manual/layers_and_masks.rst:15
msgid "Check the :ref:`layers_and_masks` for more information."
msgstr "Veja mais informações em :ref:`layers_and_masks`."
