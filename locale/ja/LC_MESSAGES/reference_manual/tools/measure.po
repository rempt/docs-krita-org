msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""

#: ../../<rst_epilog>:52
msgid ""
".. image:: images/icons/measure_tool.svg\n"
"   :alt: toolmeasure"
msgstr ""

#: ../../reference_manual/tools/measure.rst:1
msgid "Krita's measure tool reference."
msgstr ""

#: ../../reference_manual/tools/measure.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/measure.rst:11
msgid "Measure"
msgstr ""

#: ../../reference_manual/tools/measure.rst:11
msgid "Angle"
msgstr ""

#: ../../reference_manual/tools/measure.rst:11
msgid "Compass"
msgstr ""

#: ../../reference_manual/tools/measure.rst:16
msgid "Measure Tool"
msgstr ""

#: ../../reference_manual/tools/measure.rst:18
msgid "|toolmeasure|"
msgstr ""

#: ../../reference_manual/tools/measure.rst:20
msgid ""
"This tool is used to measure distances and angles. Click the |mouseleft| to "
"indicate the first endpoint or vertex of the angle, keep the button pressed, "
"drag to the second endpoint and release the button. The results will be "
"shown on the Tool Options docker. You can choose the length units from the "
"drop-down list."
msgstr ""

#: ../../reference_manual/tools/measure.rst:23
msgid "Tool Options"
msgstr ""

#: ../../reference_manual/tools/measure.rst:25
msgid ""
"The measure tool-options allow you to change between the units used. Unit "
"conversion varies depending on the DPI setting of a document."
msgstr ""
