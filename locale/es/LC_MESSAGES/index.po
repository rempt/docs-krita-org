# Spanish translations for docs_krita_org_index.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_index\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-06-19 21:35+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_userManual.jpg"
msgstr ".. image:: images/intro_page/Hero_userManual.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_tutorials.jpg"
msgstr ".. image:: images/intro_page/Hero_tutorials.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_getting_started.jpg"
msgstr ".. image:: images/intro_page/Hero_getting_started.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_reference.jpg"
msgstr ".. image:: images/intro_page/Hero_reference.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_general.jpg"
msgstr ".. image:: images/intro_page/Hero_general.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_faq.jpg"
msgstr ".. image:: images/intro_page/Hero_faq.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_resources.jpg"
msgstr ".. image:: images/intro_page/Hero_resources.jpg"

#: ../../index.rst:5
msgid "Welcome to the Krita |version| Manual!"
msgstr "¡Bienvenido al manual de Krita |version|!"

#: ../../index.rst:7
msgid "Welcome to Krita's documentation page."
msgstr "Bienvenido a la página de documentación de Krita."

#: ../../index.rst:9
msgid ""
"Krita is a sketching and painting program designed for digital artists. Our "
"vision for Development of Krita is —"
msgstr ""

#: ../../index.rst:11
msgid ""
"Krita is a free and open source cross-platform application that offers an "
"end-to-end solution for creating digital art files from scratch. Krita is "
"optimized for frequent, prolonged and focused use. Explicitly supported "
"fields of painting are illustrations, concept art, matte painting, textures, "
"comics and animations. Developed together with users, Krita is an "
"application that supports their actual needs and workflow. Krita supports "
"open standards and interoperates with other applications."
msgstr ""

#: ../../index.rst:19
msgid ""
"Krita's tools are developed keeping the above vision in mind. Although it "
"has features that overlap with other raster editors its intended purpose is "
"to provide robust tool for digital painting and creating artworks from "
"scratch. As you learn about Krita, keep in mind that it is not intended as a "
"replacement for Photoshop. This means that the other programs may have more "
"features than Krita for image manipulation tasks, such as stitching together "
"photos, while Krita's tools are most relevant to digital painting, concept "
"art, illustration, and texturing. This fact accounts for a great deal of "
"Krita's design."
msgstr ""

#: ../../index.rst:28
msgid ""
"You can download this manual as an epub `here <https://docs.krita.org/en/"
"epub/KritaManual.epub>`_."
msgstr ""

#: ../../index.rst:33
msgid ":ref:`user_manual`"
msgstr ":ref:`user_manual`"

#: ../../index.rst:33
msgid ":ref:`tutorials`"
msgstr ":ref:`tutorials`"

#: ../../index.rst:35
msgid ""
"Discover Krita’s features through an online manual. Guides to help you "
"transition from other applications."
msgstr ""
"Descubra las funcionalidades de Krita a través de un manual en línea. Le "
"sirve de guía para ayudarle a migrar de otras aplicaciones."

#: ../../index.rst:35
msgid ""
"Learn through developer and user generated tutorials to see Krita in action."
msgstr ""
"Aprenda mediante tutoriales generados por desarrolladores y usuarios para "
"ver a Krita en acción."

#: ../../index.rst:41
msgid ":ref:`getting_started`"
msgstr ":ref:`getting_started`"

#: ../../index.rst:41
msgid ":ref:`reference_manual`"
msgstr ":ref:`reference_manual`"

#: ../../index.rst:43
msgid "New to Krita and don't know where to start?"
msgstr "¿Es nuevo en Krita y no sabe por dónde empezar?"

#: ../../index.rst:43
msgid "A quick run-down of all of the tools that are available"
msgstr ""

#: ../../index.rst:48
msgid ":ref:`general_concepts`"
msgstr ":ref:`general_concepts`"

#: ../../index.rst:48
msgid ":ref:`faq`"
msgstr ":ref:`faq`"

#: ../../index.rst:50
msgid ""
"Learn about general art and technology concepts that are not specific to "
"Krita."
msgstr ""

#: ../../index.rst:50
msgid ""
"Find answers to the most common questions about Krita and what it offers."
msgstr ""

#: ../../index.rst:55
msgid ":ref:`resources_page`"
msgstr ":ref:`resources_page`"

#: ../../index.rst:55
msgid ":ref:`genindex`"
msgstr ":ref:`genindex`"

#: ../../index.rst:57
msgid ""
"Textures, brush packs, and python plugins to help add variety to your "
"artwork."
msgstr ""

#: ../../index.rst:57
msgid "An index of the manual for searching terms by browsing."
msgstr "Un índice el manual donde buscar términos."
