# Spanish translations for docs_krita_org_reference_manual___preferences___grid_settings.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___grid_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-21 03:28+0200\n"
"PO-Revision-Date: 2019-06-20 16:58+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Subdivision"
msgstr "Subdivisión"

#: ../../reference_manual/preferences/grid_settings.rst:1
msgid "Grid settings in Krita."
msgstr "Preferencias de la cuadrícula en Krita."

#: ../../reference_manual/preferences/grid_settings.rst:15
msgid "Grid Settings"
msgstr "Preferencias de la cuadrícula"

#: ../../reference_manual/preferences/grid_settings.rst:19
msgid "Deprecated in 3.0, use the :ref:`grids_and_guides_docker` instead."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:21
#, fuzzy
#| msgid "Use :guilabel:`Settings --> Configure Krita --> Grid` menu item."
msgid "Use :menuselection:`Settings --> Configure Krita --> Grid` menu item."
msgstr ""
"Use la opción del menú :guilabel:`Preferencias --> Configurar Krita --> "
"Cuadrícula`."

#: ../../reference_manual/preferences/grid_settings.rst:24
msgid ".. image:: images/preferences/Krita_Preferences_Grid.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Grid.png"

#: ../../reference_manual/preferences/grid_settings.rst:25
msgid "Fine tune the settings of the grid-tool grid here."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:28
msgid "Placement"
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:30
msgid "The user can set various settings of the grid over here."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:32
msgid "Horizontal Spacing"
msgstr "Espaciado horizontal"

#: ../../reference_manual/preferences/grid_settings.rst:33
msgid ""
"The number in Krita units, the grid will be spaced in the horizontal "
"direction."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid "Vertical Spacing"
msgstr "Espaciado vertical"

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid ""
"The number in Krita units, the grid will be spaced in the vertical "
"direction. The images below will show the usage of these settings."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:37
msgid "X Offset"
msgstr "Desplazamiento X"

#: ../../reference_manual/preferences/grid_settings.rst:38
msgid "The number to offset the grid in the X direction."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "Y Offset"
msgstr "Desplazamiento Y"

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "The number to offset the grid in the Y direction."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:42
msgid ""
"Some examples are shown below, look at the edge of the image to see the "
"offset."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid "Subdivisions"
msgstr "Subdivisiones"

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid ""
"Here the user can set the number of times the grid is subdivided. Some "
"examples are shown below."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:48
msgid "Style"
msgstr "Estilo"

#: ../../reference_manual/preferences/grid_settings.rst:50
msgid "Main"
msgstr "Principal"

#: ../../reference_manual/preferences/grid_settings.rst:51
msgid ""
"The user can set how the main lines of the grid are shown. Options available "
"are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:53
msgid ""
"The user can set how the subdivision lines of the grid are shown. Options "
"available are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""
